#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import filecmp
import os
import sys
import traceback

from numerapi.numerapi import NumerAPI


class TournamentData:
    def __init__(self):
        self.napi = NumerAPI()
        self.gather_paths()

    def gather_paths(self):
        self.now = self.napi.get_current_round()
        self.base = os.path.dirname(os.path.realpath(__file__))
        self.previous = os.path.join(self.base, str(self.now - 1) + '.zip')
        self.current = os.path.join(self.base, str(self.now) + '.zip')
        print('Current tournament: {}'.format(self.now))

    def is_current_data_present(self):
        return os.path.isfile(self.current)

    def is_previous_data_present(self):
        return os.path.isfile(self.previous)

    def download_current_data(self):
        self.napi.download_current_dataset(
            dest_filename=self.current, unzip=False)

    def is_current_and_previous_data_the_same(self):
        return filecmp.cmp(self.previous, self.current)

    def delete_current_data(self):
        os.remove(self.current)

    def load(self):
        if not self.is_current_data_present():
            print('Current data is not present')
            self.download_current_data()
            if self.is_previous_data_present():
                if self.is_current_and_previous_data_the_same():
                    self.delete_current_data()
                    print('Current data is the same as previous - skipping')
                    return False
                else:
                    print('Current data is different than previous - keeping')
                    return True
            else:
                print('Previous data is absent - keeping')
                return True
        else:
            print('Current data is present')
            return False

    def execute(self, command):
        print(command)
        code = os.system(command)
        if code != 0:
            raise Exception('Return code: {}'.format(code))

    def store(self):
        home = os.environ['HOME']
        user = os.environ['USER']
        password = os.environ['PASS']
        url = os.environ['CI_PROJECT_URL']
        with open('{}/.netrc'.format(home), 'w') as handle:
            handle.write('machine gitlab.com\n')
            handle.write('login {}\n'.format(user))
            handle.write('password {}\n'.format(password))
        self.execute('git add {}'.format(self.current))
        self.execute('git config user.email "altermarkive+bot@gmail.com"')
        self.execute('git config user.name "Bot"')
        self.execute('git commit -m "Tournament: {}" .'.format(self.now))
        self.execute('git remote set-url origin {}.git'.format(url))
        self.execute('git push')

    def prepare(self):
        self.execute('git checkout master')
        self.execute('git pull')


if __name__ == '__main__':
    try:
        data = TournamentData()
        data.prepare()
        if data.load():
            data.store()
        sys.exit(0)
    except Exception:
        print(traceback.print_exc())
        sys.exit(1)
